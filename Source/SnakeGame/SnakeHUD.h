// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "SnakeHUD.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeHUD : public AHUD
{
	GENERATED_BODY()

public:

	

protected:
	
	virtual void BeginPlay() override;
};
