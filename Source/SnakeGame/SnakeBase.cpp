// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"

FVector FirstLocation(0, 0, 0);

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	AddFood(4);
	BeginningSpeed = MovementSpeed;
	DeathOrder = 0;
	Gametimer = 0;
	PlayerScore = 0;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	(DeathOrder > 0)? SnakeDeath(DeathOrder) : Move();
	
	IsTurned = false;
	Gametimer++;
	if (Gametimer % 60 == 0) 
	{
		MovementSpeed /= 1.01;
	}

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	for(int i = 0; i < ElementsNum; i++) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		SnakeElements[SnakeElements.Num() - 1]->SetActorHiddenInGame(true);
		if(ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}
		SnakeElementsCount = SnakeElements.Num();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection) 
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->MeshX = CurrentElement->GetActorLocation().X;
		CurrentElement->MeshY = CurrentElement->GetActorLocation().Y;
		CurrentElement->SetActorHiddenInGame(false);
		PrevElement->SetActorHiddenInGame(false);
	}
	
	if (abs(SnakeElements[0]->GetActorLocation().X) >= GetGameViewportSize().Y) 
	{
		(SnakeElements[0]->GetActorLocation().X >= 0)? FirstLocation.Set(GetGameViewportSize().Y * -1, SnakeElements[0]->GetActorLocation().Y, 0): 
			FirstLocation.Set(GetGameViewportSize().Y, SnakeElements[0]->GetActorLocation().Y, 0);

		SnakeElements[0]->SetActorLocation(FirstLocation);
	}
	else if (abs(SnakeElements[0]->GetActorLocation().Y) >= GetGameViewportSize().X)
	{
		(SnakeElements[0]->GetActorLocation().Y >= 0)? FirstLocation.Set(SnakeElements[0]->GetActorLocation().X, GetGameViewportSize().X * -1, 0):
			FirstLocation.Set(SnakeElements[0]->GetActorLocation().X, GetGameViewportSize().X, 0);

		SnakeElements[0]->SetActorLocation(FirstLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector); // SetActorLocation(GetActorLocation() + MovementVector)
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) 
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
			AFood* TouchedFood = Cast<AFood>(Other);
			if (IsValid(TouchedFood)) 
			{
				AddFood();
				MovementSpeed = BeginningSpeed/float(1.01*((Gametimer/60==0)? 1 : Gametimer/60));
				PlayerScore += float(BeginningSpeed/MovementSpeed) * 10;
				switch (TouchedFood->typeNum)
				{
				case 1:
					AddSnakeElement();
					break;
				case 2:
					MovementSpeed /= 2;
					break;
				case 3:
					AddWall();
					break;
				case 4:
					PlayerScore += float(BeginningSpeed / MovementSpeed) * 10;
					break;
				}
				SetActorTickInterval(MovementSpeed);
				
				
				TouchedFood->Destroy();
				
			}
		}
	}
}

FVector2D ASnakeBase::GetGameViewportSize()
{
	FVector2D Result = FVector2D(1, 1);

	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(Result);
	}

	return Result;
}

FVector ASnakeBase::SetNewObjectLocation(float ObjSizeX, float ObjSizeY)
{
	FVector ObjLocation = FVector(1, 1, 0);
	int ObjX = rand() % int(GetGameViewportSize().Y * 2) - int(GetGameViewportSize().Y),
		ObjY = rand() % int(GetGameViewportSize().X * 2) - int(GetGameViewportSize().X);
		
	(ObjX > 0)? ObjX -= ObjSizeX : ObjX += ObjSizeX;
	(ObjY > 0) ? ObjY -= ObjSizeY : ObjY += ObjSizeY;

	float OtherObjLeftX = SnakeElements[0]->MeshX - ObjSizeX,
		OtherObjRightX = SnakeElements[0]->MeshX + ObjSizeX,
		OtherObjDownY = SnakeElements[0]->MeshY - ObjSizeY,
		OtherObjUpY = SnakeElements[0]->MeshY + ObjSizeY;
	

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		OtherObjLeftX = SnakeElements[i]->MeshX - ObjSizeX;
		OtherObjRightX = SnakeElements[i]->MeshX + ObjSizeX;
		OtherObjDownY = SnakeElements[i]->MeshY - ObjSizeY;
		OtherObjUpY = SnakeElements[i]->MeshY + ObjSizeY;

		if (ObjX > OtherObjLeftX && ObjX < OtherObjRightX && ObjY > OtherObjDownY && ObjY < OtherObjUpY)
		{
			ObjX = rand() % int(GetGameViewportSize().Y * 2) - int(GetGameViewportSize().Y);
			ObjY = rand() % int(GetGameViewportSize().X * 2) - int(GetGameViewportSize().X);
			(ObjX > 0) ? ObjX -= ObjSizeX : ObjX += ObjSizeX;
			(ObjY > 0) ? ObjY -= ObjSizeY : ObjY += ObjSizeY;
			i = SnakeElements.Num() - 1;
		}
	}

	ObjLocation.Set(ObjX, ObjY, 0);

	return ObjLocation;
}

void ASnakeBase::AddWall()
{
	FVector NewLocation(GetGameViewportSize().X, GetGameViewportSize().Y, 0);
	FTransform NewTransform(NewLocation);
	AWall* NewWall = GetWorld()->SpawnActor<AWall>(WallClass, NewTransform);
	NewLocation = SetNewObjectLocation(NewWall->MeshComponent->GetComponentScale().X*200.f, NewWall->MeshComponent->GetComponentScale().Y*200.f);
	NewWall->SetActorLocation(NewLocation);
	int32 ElemIndex = WallElements.Add(NewWall);
	float XScale = float(rand() % 4 + 1) / 2, 
		YScale = float(rand() % 4 + 1) / 2;
	
	while (XScale == YScale || XScale + 0.5 == YScale || XScale == YScale + 0.5)
	{
		XScale = float(rand() % 4 + 1) / 2;
		YScale = float(rand() % 4 + 1) / 2;
	}
	FVector NewScale(XScale, YScale, 1);
	NewWall->MeshComponent->SetWorldScale3D(NewScale);
	
}

void ASnakeBase::AddFood(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++) 
	{
		FVector NewLocation(GetGameViewportSize().X, GetGameViewportSize().Y, 0);
		FTransform NewTransform(NewLocation);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
		NewLocation = SetNewObjectLocation(NewFood->GetActorScale().X*100.f, NewFood->GetActorScale().Y * 100.f);
		NewFood->SetActorLocation(NewLocation);
		int32 ElemIndex = FoodElements.Add(NewFood);
	}
}

void ASnakeBase::SnakeDeath(int elem)
{
	if (DeathOrder < SnakeElements.Num())
	{
		SnakeElements[DeathOrder]->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SnakeElements[DeathOrder]->Destroy();
		DeathOrder++;
	}
	else
	{
		Destroy();
	}
}



