// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Food.h"
#include "Wall.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	float BeginningSpeed;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float PlayerScore = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int DeathOrder = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int	Gametimer = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int SnakeElementsCount = 0;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY()
	bool IsTurned = false;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWall> WallClass;

	UPROPERTY(EditDefaultsOnly)
	TArray<AWall*> WallElements;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TArray<AFood*> FoodElements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void SnakeDeath(int elem);
	UFUNCTION()
	FVector2D GetGameViewportSize();
	UFUNCTION()
	FVector SetNewObjectLocation(float ObjSizeX = 60.f, float ObjSizeY = 60.f);
	UFUNCTION()
	void AddWall();
	UFUNCTION()
	void AddFood(int ElementsNum = 1);
};
